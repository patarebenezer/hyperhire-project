import { useEffect } from "react";
import { useAtom, useSetAtom } from "jotai";
import { categoryAtomStorage } from "@/modules/common/utils/mainJotai";

export default function BannerCategory() {
 const setOpenCategory = useSetAtom(categoryAtomStorage);
 const [openCategory] = useAtom(categoryAtomStorage);
 const handleResize = () => {
  if (window.innerWidth > 768) {
   setOpenCategory(false);
  }
 };

 useEffect(() => {
  window.addEventListener("resize", handleResize);
 }, []);

 return (
  <div className='relative z-5'>
   <div className='flex gap-2 justify-center items-center ml-4'>
    <svg
     onClick={() => setOpenCategory(!openCategory)}
     className='w-4 h-6 text-primary cursor-pointer'
     fill='none'
     stroke='currentColor'
     xmlns='http://www.w3.org/2000/svg'
    >
     <path
      strokeLinecap='round'
      strokeLinejoin='round'
      strokeWidth='1.5'
      d='M4 8h16M4 12h16M4 16h16'
     ></path>
    </svg>
    <p className='text-sm antialiased'>Category</p>
   </div>
  </div>
 );
}
