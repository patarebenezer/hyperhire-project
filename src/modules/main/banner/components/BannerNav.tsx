"use client";
import Image from "next/image";
import BannerSearch from "./BannerSearch";
import BannerCategory from "./BannerCategory";

export default function BannerNav() {
 return (
  <>
   <div className='flex items-center relative z-50'>
    <Image
     src='https://www.testvalley.kr/logo/logo-new.svg'
     alt='logo company'
     width={125}
     height={125}
    />
    <div className='hidden lg:block relative w-full z-50'>
     <BannerCategory />
    </div>
   </div>
   <div className='lg:w-1/3 my-auto'>
    <BannerSearch />
   </div>
   <div className='hidden lg:flex gap-2 items-center'>
    <Image
     src='https://www.testvalley.kr/common/home-event.svg'
     alt='disc icon'
     width={28}
     height={28}
     className='cursor-pointer'
    />
    <span className='font-light text-gray-300'>|</span>
    <p className='text-sm cursor-pointer'>Login / Sign up</p>
   </div>
  </>
 );
}
