/* eslint-disable @next/next/no-img-element */
import { useState } from "react";

const BannerSearch = () => {
 const [isOpenRecentBox, setIsOpenRecentBox] = useState(false);
 const handleOpenRecentBox = () => {
  setIsOpenRecentBox(true);
  setTimeout(() => {
   setIsOpenRecentBox(false);
  }, 3000);
 };

 return (
  <>
   <div className='lg:hidden flex gap-3'>
    <img src='https://www.testvalley.kr/common/bell_default.svg' alt='bell' />
    <img src='https://www.testvalley.kr/common/search_new.svg' alt='search' />
   </div>
   <div className='hidden lg:block relative z-50'>
    <div className='absolute inset-y-0 left-0 pl-2 flex items-center pointer-events-none'>
     <svg
      className='text-gray-600 h-5 w-5 fill-current'
      xmlns='http://www.w3.org/2000/svg'
      viewBox='0 0 24 24'
     >
      <path d='M15.5 14h-.79l-.28-.27a6.5 6.5 0 0 0 1.48-5.34c-.47-2.78-2.79-5-5.59-5.34a6.505 6.505 0 0 0-7.27 7.27c.34 2.8 2.56 5.12 5.34 5.59a6.5 6.5 0 0 0 5.34-1.48l.27.28v.79l4.25 4.25c.41.41 1.08.41 1.49 0 .41-.41.41-1.08 0-1.49L15.5 14zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z' />
     </svg>
    </div>
    <input
     onClick={handleOpenRecentBox}
     type='text'
     placeholder="If you're wondering whether to buy it or not,"
     className='border w-full h-10 pl-8 pr-5 rounded font-light text-sm focus: outline-primary'
    />
    {isOpenRecentBox && (
     <div className='absolute w-full my-2 px-4 py-5 bg-white border rounded block'>
      <p className='font-bold antialiased'>Recent searches</p>
      <p className='font-light text-sm mt-5'>There are no recent searches</p>
     </div>
    )}
   </div>
  </>
 );
};

export default BannerSearch;
