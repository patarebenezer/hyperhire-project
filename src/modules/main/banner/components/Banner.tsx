/* eslint-disable @next/next/no-img-element */
import React from "react";
import { useRef } from "react";
import { useAtom } from "jotai";
import { useMainBannerData } from "@/services/api/testvalley.hooks";
import { categoryAtomStorage } from "@/modules/common/utils/mainJotai";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import BtnNext from "@/modules/main/components/BtnNext";
import BtnPrev from "@/modules/main/components/BtnPrev";

export default function Banner() {
 const { data, isLoading } = useMainBannerData();
 const [openCategory] = useAtom(categoryAtomStorage);

 const slider = useRef<Slider | null>(null);

 const next = () => {
  slider?.current?.slickNext();
 };

 const prev = () => {
  slider?.current?.slickPrev();
 };

 const settings = {
  arrows: false,
  className: "center",
  centerMode: true,
  infinite: true,
  speed: 500,
  autoplay: true,
  slidesToShow: 1,
  centerPadding: "15%",
  dots: false,
  responsive: [
   {
    breakpoint: 960,
    settings: {
     centerPadding: "0%",
    },
   },
  ],
 };

 if (isLoading) {
  return <p className='text-center mt-5'>Loading..</p>;
 }
 return (
  <div className={`relative ${openCategory ? "-z-10" : "z-20"}`}>
   <Slider ref={slider} {...settings}>
    {Array.isArray(data)
     ? data?.map((item, index) => (
        <div className='flex items-center justify-center' key={index}>
         <div
          key={item.id}
          className='flex gap-4 mx-0 lg:mx-3 flex-col items-center rounded-lg hover:bg-blue-100 dark:bg-gray-800 lg:flex-row'
         >
          <div className='flex justify-center items-center'>
           <img
            src={item?.pcImageUrl}
            alt='banner'
            className='w-full h-[13rem] lg:h-full'
           />
          </div>
         </div>
        </div>
       ))
     : []}
   </Slider>
   <BtnPrev
    onClick={prev}
    clsName='hidden lg:block slider-button rounded-full bg-[rgba(51,51,51,0.5)] w-[44px] h-[44px] absolute top-36 left-[18%] text-white'
   />
   <BtnNext
    onClick={next}
    clsName='hidden lg:block slider-button bg-[rgba(51,51,51,0.5)] rounded-full w-[44px] h-[44px] absolute top-36 right-[18%] text-white'
   />
  </div>
 );
}
