import React from "react";
import BannerNav from "./components/BannerNav";

export default function MainBanner() {
 return (
  <div className='justify-between lg:w-[70%] w-full flex px-4 lg:px-0'>
   <BannerNav />
  </div>
 );
}
