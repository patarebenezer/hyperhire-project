import React from "react";

export type BtnType = {
 onClick: () => void;
 clsName?: string;
};

export default function BtnNext({ onClick, clsName }: BtnType) {
 return (
  <>
   <button className={clsName} onClick={onClick}>
    <svg
     xmlns='http://www.w3.org/2000/svg'
     className='h-6 w-6 mx-auto'
     fill='none'
     viewBox='0 0 24 24'
     stroke='currentColor'
    >
     <path
      strokeLinecap='round'
      strokeLinejoin='round'
      strokeWidth={1}
      d='M9 5l7 7-7 7'
     />
    </svg>
   </button>
  </>
 );
}
