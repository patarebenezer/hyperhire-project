/* eslint-disable @next/next/no-img-element */
import React from "react";
import { useMainShortcutData } from "@/services/api/testvalley.hooks";

export default function MainShortcut() {
 const { data, isLoading } = useMainShortcutData();
 if (isLoading) {
  return <p className='text-center mt-5'>Loading shortcuts..</p>;
 }
 return (
  <div className='grid grid-cols-5 lg:grid-cols-10 gap-5 justify-around my-10 px-5 lg:px-0'>
   {Array.isArray(data)
    ? data?.map((item, index) => (
       <div key={index} className='grid justify-center gap-2'>
        <div className='flex justify-center'>
         <img
          className='cursor-pointer lg:w-[60px] lg:h-[60px] w-[45px] h-[45px]'
          src={item?.imageUrl}
          alt='icon'
         />
        </div>
        <p className='text-center text-[0.7rem] font-light antialiased'>
         {item?.title}
        </p>
       </div>
      ))
    : []}
  </div>
 );
}
