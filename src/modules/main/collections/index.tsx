import { useCollectionsData } from "@/services/api/testvalley.hooks";
import React from "react";
import { Collections } from "@/types/testvalley";
import CollectionsSlider from "@/modules/main/collections/components/CollectionsSlider";

export default function MainCollections() {
 const { data, isLoading } = useCollectionsData();
 if (isLoading) {
  return <p className='text-center my-5'>Loading collections data..</p>;
 }
 return (
  <>
   <CollectionsSlider data={data as Collections[]} />
  </>
 );
}
