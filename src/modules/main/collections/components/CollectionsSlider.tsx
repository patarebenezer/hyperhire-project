/* eslint-disable @next/next/no-img-element */
import React, { useRef } from "react";
import CollectionDeals, { CollectionDealsType } from "./CollectionDeals";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import BtnNext from "@/modules/main/components/BtnNext";
import BtnPrev from "@/modules/main/components/BtnPrev";
import { Collections } from "@/types/testvalley";

export default function CollectionsSlider({ data }: CollectionDealsType) {
 const slider = useRef<Slider | null>(null);

 const next = () => {
  slider?.current?.slickNext();
 };

 const prev = () => {
  slider?.current?.slickPrev();
 };

 const settings = {
  infinite: false,
  speed: 600,
  slidesToShow: 4,
  slidesToScroll: 3,
  autoplay: true,
  className: "left",
 };

 const renderSliderContent = (item: Collections, index: number) => (
  <div key={index} className='flex gap-4'>
   <div className='w-1/5 lg:block hidden relative'>
    <h1 className='font-semibold antialiased text-xl lg:text-2xl'>
     {item?.title}
    </h1>
    <p className='text-[12px] lg:text-sm font-light text-gray-500 my-2'>
     {item?.subtitle}
    </p>
    <div className='absolute bottom-10 lg:block hidden'>
     <BtnPrev onClick={prev} />
     <BtnNext onClick={next} />
    </div>
   </div>
   <div key={index} className='w-full hidden lg:block lg:w-[75%]'>
    <Slider ref={slider} {...settings} className='slider-container my-8'>
     {item?.items?.map((value, i) => (
      <div key={i} className='w-full px-4'>
       <div className='relative'>
        <img
         src={value?.publication?.media[0]?.uri}
         alt='media'
         className='rounded w-[174px]'
        />
        {value?.publication?.tagsOnImage.length > 0 && (
         <div className='bg-[#019E89] flex gap-1 w-fit absolute bottom-0 text-white text-[12px] p-1'>
          <img
           src='https://www.testvalley.kr/common/return-new.svg'
           alt='tags'
          />
          <p>{value?.publication?.tagsOnImage}</p>
         </div>
        )}
       </div>
       <p>{value?.publication?.productName}</p>
       <p className='my-2'>
        {value?.publication?.priceInfo?.discountRate && (
         <span className='text-red-500'>
          {value?.publication?.priceInfo?.discountRate}%
         </span>
        )}
        {value?.publication?.priceInfo?.price.toLocaleString()}
        <span className='text-[10px]'>원</span>
        <p className='flex items-center'>
         <span>
          <img
           src='https://www.testvalley.kr/star/star-darkgray.svg'
           alt='star'
          />
         </span>
         <span className='text-[10px] my-2'>{value?.publication?.rating}</span>
        </p>
        {value?.publication?.prefaceIconUrl && (
         <div className='border p-1 gap-1 flex items-center w-fit'>
          <img
           src={value.publication?.prefaceIconUrl}
           alt='preface icon'
           width={15}
          />
          <span className='text-sm'>{value?.publication?.preface}</span>
         </div>
        )}
       </p>
      </div>
     ))}
    </Slider>
   </div>
  </div>
 );
 return (
  <>
   <div className='w-full py-8'>
    <div className='lg:hidden block -mt-10'>
     <CollectionDeals data={data} />
    </div>
    {Array.isArray(data)
     ? data.map((item, index) =>
        [1197, 2431, 2430, 2433, 2315, 1151, 2117, 2359].includes(
         item?.id as number
        )
         ? renderSliderContent(item, index)
         : null
       )
     : null}
   </div>
  </>
 );
}
