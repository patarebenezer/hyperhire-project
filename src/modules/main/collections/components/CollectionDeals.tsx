/* eslint-disable @next/next/no-img-element */
import React from "react";
import { Collections } from "@/types/testvalley";

export type CollectionDealsType = {
 data: Collections[];
};
export default function CollectionDeals({ data }: CollectionDealsType) {
 const renderContent = (item: Collections, index: number) => (
  <>
   <div className='w-full my-10'>
    <h1 className='font-semibold antialiased text-xl lg:text-2xl'>
     {item?.title}
    </h1>
    <p className='text-[12px] lg:text-sm font-light text-gray-500 my-2'>
     {item?.subtitle}
    </p>
   </div>
   <div key={index} className='grid grid-cols-2 lg:grid-cols-4 gap-6'>
    {item?.items?.map((value, i) => (
     <div key={i} className='w-[174px]'>
      <img
       src={value?.publication?.media[0]?.uri}
       alt='media'
       className='rounded'
      />
      <p>{value?.publication?.productName}</p>
      <p>
       {value?.publication?.priceInfo?.discountRate && (
        <span className='text-red-500'>
         {value?.publication?.priceInfo?.discountRate}%
        </span>
       )}

       {value?.publication?.priceInfo?.price.toLocaleString()}
       <span className='text-[10px]'>원</span>
       <p className='flex items-center'>
        <span>
         <img
          src='https://www.testvalley.kr/star/star-darkgray.svg'
          alt='star'
         />
        </span>
        <span className='text-[10px] my-2'>{value?.publication?.rating}</span>
       </p>
       {value?.publication?.prefaceIconUrl && (
        <div className='border p-1 gap-1 flex items-center w-fit'>
         <img
          src={value.publication?.prefaceIconUrl}
          alt='preface icon'
          width={15}
         />
         <span className='text-sm'>{value?.publication?.preface}</span>
        </div>
       )}
      </p>
     </div>
    ))}
   </div>
  </>
 );
 return (
  <div className='w-full'>
   {Array.isArray(data)
    ? data.map((item, index) =>
       [1197, 2431, 2430, 2433, 2315, 1151, 2117, 2359].includes(
        item?.id as number
       )
        ? renderContent(item, index)
        : null
      )
    : null}
  </div>
 );
}
