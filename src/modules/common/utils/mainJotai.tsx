import { atomWithStorage } from "jotai/utils";

export const categoryAtomStorage = atomWithStorage("category", false);
