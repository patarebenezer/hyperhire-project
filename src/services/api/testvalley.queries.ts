import { createQueryKeys } from "@lukemorales/query-key-factory";
import {
 getCollectionsData,
 getMainBannerData,
 getMainShortcutData,
} from "@/services/api/testvalley.service";

export const testValleyMasterQueries = createQueryKeys("testValleyMaster", {
 mainBanner: () => ({
  queryFn: () => getMainBannerData(),
  queryKey: ["mainBanner"],
 }),
 mainShortcut: () => ({
  queryFn: () => getMainShortcutData(),
  queryKey: ["mainShortcut"],
 }),
 collections: () => ({
  queryFn: () => getCollectionsData(),
  queryKey: ["collections"],
 }),
});
