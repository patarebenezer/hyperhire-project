import { useQuery } from "@tanstack/react-query";
import { testValleyMasterQueries } from "@/services/api/testvalley.queries";

export const useMainBannerData = () => {
 return useQuery({
  ...testValleyMasterQueries.mainBanner(),
 });
};

export const useMainShortcutData = () => {
 return useQuery({
  ...testValleyMasterQueries.mainShortcut(),
 });
};

export const useCollectionsData = () => {
 return useQuery({
  ...testValleyMasterQueries.collections(),
 });
};
