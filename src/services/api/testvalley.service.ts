import { axiosClient } from "@/utils/axiosClient";
import { clientEnv } from "@/services/env/client.environment";
import {
 GetCollectionsResponse,
 GetMainBannerResponse,
} from "@/types/testvalley";

export const getMainBannerData = async () => {
 try {
  const response = await axiosClient.get<GetMainBannerResponse>(
   `${clientEnv.API_BASE_URL}main-banner/all`
  );
  return response.data;
 } catch (error) {
  console.error("error", error);
  throw error;
 }
};

export const getMainShortcutData = async () => {
 try {
  const response = await axiosClient.get<GetMainBannerResponse>(
   `${clientEnv.API_BASE_URL}main-shortcut/all`
  );
  return response.data;
 } catch (error) {
  console.error("error", error);
  throw error;
 }
};

export const getCollectionsData = async () => {
 try {
  const response = await axiosClient.get<GetCollectionsResponse>(
   `${clientEnv.API_BASE_URL}collections?prearrangedDiscount`
  );
  return response.data.items;
 } catch (error) {
  console.error("error", error);
  throw error;
 }
};
