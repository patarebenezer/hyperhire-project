const zod = require("zod");

const clientSchema = zod.object({
 API_BASE_URL: zod.string(),
});

module.exports = {
 clientSchema,
};
