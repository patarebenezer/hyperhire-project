"use client";

import React from "react";
import {
 dehydrate,
 Hydrate,
 QueryClient,
 QueryClientProvider,
} from "@tanstack/react-query";
import { Provider as JotaiProvider } from "jotai";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";

export default function Providers({
 children,
}: Readonly<React.PropsWithChildren>) {
 // set up query client
 const [queryClient] = React.useState(
  () =>
   new QueryClient({
    defaultOptions: {
     queries: {
      refetchOnWindowFocus: false,
     },
    },
   })
 );

 const dehydratedState = dehydrate(queryClient);

 return (
  <QueryClientProvider client={queryClient}>
   <Hydrate state={dehydratedState}>
    <JotaiProvider>{children}</JotaiProvider>
   </Hydrate>
   <ReactQueryDevtools initialIsOpen={false} />
  </QueryClientProvider>
 );
}
