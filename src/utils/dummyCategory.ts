export const dummyCategory = [
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1686792332259-%E1%84%8B%E1%85%A2%E1%84%91%E1%85%B3%E1%86%AF.jpg",
  menuName: "Apple",
 },
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1685955025527-%E1%84%80%E1%85%A6%E1%84%8B%E1%85%B5%E1%86%B7%E1%84%80%E1%85%B5%E1%84%80%E1%85%B5.jpg",
  menuName: "Game Device",
 },
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1685955030888-%E1%84%89%E1%85%B3%E1%84%86%E1%85%A1%E1%84%90%E1%85%B3%E1%84%91%E1%85%A9%E1%86%AB.jpg",
  menuName: "Smartphone",
 },
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1685955037712-%E1%84%89%E1%85%B3%E1%84%86%E1%85%A1%E1%84%90%E1%85%B3%E1%84%8B%E1%85%AF%E1%84%8E%E1%85%B5.jpg",
  menuName: "Smartwatch",
 },
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1685955044484-%E1%84%90%E1%85%A2%E1%84%87%E1%85%B3%E1%86%AF%E1%84%85%E1%85%B5%E1%86%BA.jpg",
  menuName: "Tablet",
 },
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1685955050510-%E1%84%8B%E1%85%B3%E1%86%B7%E1%84%92%E1%85%A3%E1%86%BC%E1%84%80%E1%85%B5%E1%84%80%E1%85%B5.jpg",
  menuName: "Audio Equipment",
 },
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1685955056544-%E1%84%8F%E1%85%A1%E1%84%86%E1%85%A6%E1%84%85%E1%85%A1.jpg",
  menuName: "Camera",
 },
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1685955066512-%E1%84%8F%E1%85%A5%E1%86%B7%E1%84%91%E1%85%B2%E1%84%90%E1%85%A5.jpg",
  menuName: "Computer",
 },
 {
  urlImage:
   "https://dvd6ljcj7w3pj.cloudfront.net/static/CATEGORY/1685955072059-%E1%84%8B%E1%85%A7%E1%86%BC%E1%84%89%E1%85%A1%E1%86%BC%E1%84%80%E1%85%B5%E1%84%80%E1%85%B5.jpg",
  menuName: "Video Equipment",
 },
];
