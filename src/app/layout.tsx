import type { Metadata } from "next";

import "@/app/globals.css";
import Providers from "@/utils/provider";

export const metadata: Metadata = {
 title: "Hyperhire",
 description: "Assigment",
};

export default function RootLayout({
 children,
}: Readonly<{
 children: React.ReactNode;
}>) {
 return (
  <html lang='en' data-theme='light' suppressHydrationWarning={true}>
   <body className={"bg-[#F6FAFB]"}>
    <Providers>
     <div className='w-[440px] lg:w-full mx-auto bg-white'>{children}</div>
    </Providers>
   </body>
  </html>
 );
}
