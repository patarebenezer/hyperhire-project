"use client";
import React from "react";

import MainCollections from "@/modules/main/collections";
import MainBanner from "@/modules/main/banner";
import MainShortcut from "@/modules/main/shortcut";
import { useAtom } from "jotai";
import { categoryAtomStorage } from "@/modules/common/utils/mainJotai";
import { dummyCategory } from "@/utils/dummyCategory";
import Image from "next/image";
import Banner from "@/modules/main/banner/components/Banner";

export default function Home() {
 const [openCategory] = useAtom(categoryAtomStorage);

 return (
  <>
   <div className='lg:border h-[4.5rem] flex justify-around z-50'>
    <MainBanner />
   </div>
   {openCategory && (
    <div className='w-full border-b py-8 fixed bg-white '>
     <div className='w-[70%] mx-auto flex justify-between'>
      {dummyCategory.map((item, index) => (
       <div key={index} className='grid justify-center gap-4'>
        <div className='flex justify-center'>
         <Image
          className='cursor-pointer'
          src={item?.urlImage}
          alt='icon'
          width={70}
          height={70}
         />
        </div>
        <p className='text-center antialiased'>{item?.menuName}</p>
       </div>
      ))}
     </div>
    </div>
   )}

   <div className='-z-50'>
    <Banner />
   </div>
   <div className='w-full px-4 lg:px-0 lg:w-[70%] mx-auto'>
    <MainShortcut />
    <MainCollections />
   </div>
  </>
 );
}
