import { ApiResponseSuccess } from "../api";

export type GetMainBannerResponse = ApiResponseSuccess<MainBanner>;
export type GetMainShortcutResponse = ApiResponseSuccess<MainShortcut>;
export type GetCollectionsResponse = ApiResponseSuccess<Collections>;

export type MainBanner = Partial<{
 mainBannerId: number;
 title: string;
 sort: number;
 pcImageUrl: string;
 mobileImageUrl: string;
 linkUrl: string;
 startDate: string;
 endDate: string;
 creator: string;
 updater: string;
 deleter: null;
 createdAt: string;
 updatedAt: string;
 deletedAt: null;
}>;

export type MainShortcut = Partial<{
 mainShortcutId: number;
 title: string;
 sort: number;
 imageUrl: string;
 linkUrl: string;
 creator: string;
 updater: string;
 deleter: null;
 createdAt: string;
 updatedAt: string;
 deletedAt: null;
}>;

export type CollectionsMedia = Partial<{
 createdAt: string;
 updatedAt: string;
 deletedAt: null;
 uuid: string;
 mimeType: string;
 uri: string;
 fileName: string;
 objectKey: string;
 deviceType: null;
 collectionId: number;
 seq: number;
 itemKey: null;
 type: string;
}>;

export type CollectionsThumbnail = Partial<{
 createdAt: string;
 updatedAt: string;
 deletedAt: null;
 uuid: string;
 mimeType: string;
 uri: string;
 fileName: string;
 objectKey: string;
 deviceType: null;
 collectionId: number;
 seq: number;
 itemKey: null;
 type: string;
}>;

export type Collections = Partial<{
 id: number;
 type: string;
 code: string;
 title: string;
 subtitle: string;
 description: string;
 trialPeriod: null;
 installmentPrice: null;
 installmentPeriod: null;
 rating: number;
 startDate: null;
 endDate: null;
 viewType: null;
 createdAt: string;
 items: [
  {
   publication: {
    title: string;
    code: string;
    brandName: string;
    productName: string;
    media: [
     {
      seq: number;
      type: string;
      uri: string;
      mimeType: string;
      deviceType: null;
     }
    ];
    tagsOnImage: [string];
    rating: string;
    preface: string;
    prefaceIconUrl: string;
    priceInfo: {
     price: number;
     discountPrice: number;
     discountRate: number;
    };
    tagsOnDesc: [string];
    discounts: [
     {
      id: number;
      name: string;
      type: string;
      calcMethod: string;
      value: number;
      activeFrom: null;
      activeTo: null;
      qty: number;
      remain: null;
     }
    ];
   };
  }
 ];
 media: [CollectionsMedia];
 thumbnail: CollectionsThumbnail;
 taggings: [];
 singleCollections: [];
}>;
